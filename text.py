from Classes.enums import Field

# english text
englishText = dict()
englishText[Field.REACTION_TIME] = "Time of reaction [s]"
englishText[Field.HOW_MANY] = "How many turns does the particle beam " \
                    "\ntravel around the LHC tunnel?"
englishText[Field.DATE] = "Score date"
englishText[Field.TOP_SCORES] = "Top 3 Scores"
englishText[Field.TOTAL_SCORES] = " scores total)"
englishText[Field.LAST_SCORE] = "Last score"

englishText[Field.INSTRUCTION_IDLE] = "\nThis device will test your reaction time. \n" \
                    "If the green light is on, you may start the game. " \
                    "Turn red knob clockwise, \nuntil it jumps up. " \
                    "After that you will see the yellow light. Prepare for the test!"
englishText[Field.INSTRUCTION_PREPARING] = "The game started! Wait a few seconds for the red light \n" \
                    " - once you see it, press hard the red knob as fast as you can!"
englishText[Field.INSTRUCTION_RUNNING] = "Push the red button now!"
englishText[Field.INSTRUCTION_STOP] = "Your reaction time was: "
englishText[Field.INSTRUCTION_TIMEOUT] = "The waiting time for reaction was too long, try again! \n" \
                                         "Press the button. "
englishText[Field.INSTRUCTION_FALSE_START] = "You pressed the red knob too early! \n" \
                                                  "Wait for the green light and start again."
englishText[Field.INSTRUCTION_ERROR] = "ERROR"

englishText[Field.CHART_TITLE] = "Score histogram"
englishText[Field.CHART_X] = "Reaction time [s]"
englishText[Field.CHART_Y] = "Number of results"

# french text
frenchText = dict()
frenchText[Field.REACTION_TIME] = "Temps de r\u00E9action [s]"
frenchText[Field.HOW_MANY] = "Combien de fois le faisceau de particules \nparcourt-il " \
                    "le tour du tunnel du LHC?"
frenchText[Field.DATE] = "Date du score"
frenchText[Field.TOP_SCORES] = "3 meilleurs scores"
frenchText[Field.TOTAL_SCORES] = " résultats au total)"
frenchText[Field.LAST_SCORE] = "Dernier score"

frenchText[Field.INSTRUCTION_IDLE] = "Cet appareil testera votre temps de r\u00E9action. \n"\
                     "Si le voyant vert est allumé, vous pouvez commencer le jeu. \n" \
                     "Tournez le bouton rouge dans le sens des aiguilles d'une montre, jusqu'à ce qu'il monte. \n" \
                     "Après cela, vous verrez la lumière jaune. Pr\u00E9parez-vous pour le test!"
frenchText[Field.INSTRUCTION_PREPARING] = "Le jeu a commencé! Attendez quelques secondes pour le feu rouge \n" \
                     "- Si vous le voyez, pressez fort le bouton rouge aussi vite que vous le pouvez!"
frenchText[Field.INSTRUCTION_RUNNING] = "Appuyez sur le bouton rouge maintenant!"
frenchText[Field.INSTRUCTION_STOP] = "Votre temps de réaction était: "
frenchText[Field.INSTRUCTION_TIMEOUT] = "Le temps d'attente pour la réaction était trop long, essayez encore! \n "\
                                          "Appuie sur le bouton."
frenchText[Field.INSTRUCTION_FALSE_START] = "Vous avez appuyé sur le bouton rouge trop tôt! \n " \
                                            "Attendez le feu vert et recommencez."
frenchText[Field.INSTRUCTION_ERROR] = "ERROR"

frenchText[Field.CHART_TITLE] = "Score histogramme"
frenchText[Field.CHART_X] = "Temps de r\u00E9action"
frenchText[Field.CHART_Y] = "Nombre de r\u00E9sultats"

# polish text
polishText = dict()
polishText[Field.REACTION_TIME] = "Czas reakcji [s]"
polishText[Field.HOW_MANY] = "Ile razy wiązka zdąży okrążyć " \
                    "\ntunel LHC?"
polishText[Field.DATE] = "Data"
polishText[Field.TOP_SCORES] = "Top 3 Wyników"
polishText[Field.TOTAL_SCORES] = " wyników do tej pory)"
polishText[Field.LAST_SCORE] = "Ostatni Wynik"

polishText[Field.INSTRUCTION_IDLE] = "\nTo urządzenie zmierzy twój czas reakcji.  \n" \
                    "Jeżeli świeci się zielone światło, możesz rozpocząć.  " \
                    "Obróć czerwony przycisk w prawo, \naż odskoczy. " \
                    "Następnie powinno się zapalić żółte światło. Przygotuj się na test. "
polishText[Field.INSTRUCTION_PREPARING] = "Poczekaj kilka sekund na czerwone światło \n" \
                    " - kiedy je zobaczysz wciśnij przycik tak szybko jak tylko potrafisz! "
polishText[Field.INSTRUCTION_RUNNING] = "Wciśnij czerwony przycisk!"
polishText[Field.INSTRUCTION_STOP] = "Twój czas reakcji to: "
polishText[Field.INSTRUCTION_TIMEOUT] = "Przycisk nie został wciśnięty, spróbuj ponownie.  \n" \
                                         "Naciśnij przycisk. "
polishText[Field.INSTRUCTION_FALSE_START] = "Przycisk został wciśnięty zbyt wcześnie! \n" \
                                                  "Poczekaj na zielone światło i zacznij od nowa."
polishText[Field.INSTRUCTION_ERROR] = "ERROR"

polishText[Field.CHART_TITLE] = "Histogram Wyników"
polishText[Field.CHART_X] = "Czas reakcji [s]"
polishText[Field.CHART_Y] = "Liczba Wyników"
