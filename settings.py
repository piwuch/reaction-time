from Classes.enums import Language, Field

default_width = 20
middle_width = 79-default_width
chart_size_x = 10.25
chart_size_y = 4
fontsize = 18
language = Language.POLISH
# language_switching_time = 2 # in seconds
arduino_port = "/dev/ttyACM0"
database_file = "/home/pi/Py/reaction-time/Data/score_board.csv"

colors = dict()
colors[Field.REACTION_TIME] =           "SteelBlue3"
colors[Field.HOW_MANY] =                "SteelBlue2"
colors[Field.DATE] =                    "deep sky blue"
colors[Field.SCORE_REACTION_TIME] =     "SteelBlue2"
colors[Field.SCORE_HOW_MANY] =          "SteelBlue1"
colors[Field.SCORE_DATE] =              "sky blue"
colors[Field.TOP_SCORES] =              "steel blue"
colors[Field.LAST_SCORE] =              "steel blue"
colors[Field.INSTRUCTION_IDLE] =        "lawn green"
colors[Field.INSTRUCTION_PREPARING] =   "gold"
colors[Field.INSTRUCTION_RUNNING] =     "red2"
colors[Field.INSTRUCTION_STOP] =        "white"
colors[Field.INSTRUCTION_TIMEOUT] =     "gray60"
colors[Field.INSTRUCTION_FALSE_START] = "gray60"
colors[Field.INSTRUCTION_ERROR] =       "gray5"
