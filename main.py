from tkinter import *

from Classes.Gui import Gui
from Classes.LanguageBox import LanguageBox
import settings
from Classes.enums import Language

root = Tk()

if len(sys.argv) > 1:
    if sys.argv[1] == "english":
        settings.language = Language.ENGLISH
    elif sys.argv[1] == "french":
        settings.language = Language.FRENCH
    elif sys.argv[1] == "polish":
        settings.language = Language.POLISH
    else:
        settings.language = Language.ENGLISH
    root = Tk()
    gui = Gui(root, settings.language)
    root.mainloop()
else:
    print("language argument should be passed!")


# gui = LanguageBox(root)
# root.mainloop()
# print(settings.language)


