from tkinter import *
import settings
from Classes.enums import Language

class LanguageBox(object):
    def __init__(self, master):
        self.master = master
        B1 = Button(master, text = "English", command = lambda: self.change(Language.ENGLISH))
        B1.pack()
        B2 = Button(master, text = "French", command = lambda: self.change(Language.FRENCH))
        B2.pack()

    def change(self, languageSet=Language.POLISH):
        settings.language = languageSet
        self.master.destroy()

