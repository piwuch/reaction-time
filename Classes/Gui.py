from tkinter import *
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from pandas import DataFrame
from Classes.FileManager import FileManager
from settings import *
from text import *
from serial import Serial


class Gui(object):
    def __init__(self, master, language):
        #language setting
        self.text = dict()
        if language == Language.ENGLISH:
            self.text = englishText
        elif language == Language.FRENCH:
            self.text = frenchText
        elif language == Language.POLISH:
            self.text = polishText

        # grid marker
        row = 0
        # score base
        self.db = FileManager(filename=database_file)

        # main window
        self.master = master
        self.master.geometry("{0}x{1}+0+0".format(self.master.winfo_screenwidth(), self.master.winfo_screenheight()))
        self.master.attributes('-fullscreen', True)

        # instructions
        self.instruction_row = row
        self.placeLabel(self.master, self.text[Field.INSTRUCTION_IDLE], "yellow", width=44, row=self.instruction_row)
        row += 1

        # histogram
        self.histogramFrame = Frame(self.master)
        self.histogramFrame.grid(row=row, column=0, sticky="NSEW")
        self.histogram_row = row
        row += 1
        self.refresh_histogram()

        # frame for column names
        self.columnsFrame = Frame(self.master)
        self.columnsFrame.grid(row=row, column=0, sticky="NSEW")
        row += 1
        self.placeLabel(self.columnsFrame, self.text[Field.REACTION_TIME], colors[Field.REACTION_TIME])
        self.placeLabel(self.columnsFrame, self.text[Field.HOW_MANY], colors[Field.HOW_MANY], width=middle_width, column=1)
        #self.placeLabel(self.columnsFrame, self.text[Field.DATE], colors[Field.DATE], column=2)
        
        # last scores
        self.placeLabel(self.master, self.text[Field.LAST_SCORE], colors[Field.LAST_SCORE], row=row)
        row += 1
        self.lastFrame = Frame(self.master)
        self.lastFrame.grid(row=row, column=0, sticky="NSEW")
        row += 1
        self.refresh_last()
        # top scores
        self.top_scores_row = row
        self.placeLabel(self.master,
                        text=self.text[Field.TOP_SCORES] + " (" + str(self.db.get_number_of_results()) + self.text[
                            Field.TOTAL_SCORES],
                        background=colors[Field.TOP_SCORES],
                        width=default_width,
                        fontsize=fontsize,
                        row=self.top_scores_row)
        row += 1
        self.topFrame = Frame(self.master)
        self.topFrame.grid(row=row, column=0, sticky="NSEW")
        row += 1
        self.refresh_top()

        self.master.bind('<Escape>', self.exit)
        self.i = 0
        # arduino
        self.arduino = Serial(arduino_port, 115200)
        self.master.after(1000, self.process)

    # read and display 3 top scores
    def refresh_top(self):
        temp = self.db.get_top()
        for i in range(3):
            for j in range(2):
                if j == 0:
                    color = colors[Field.SCORE_REACTION_TIME]
                    width = default_width
                elif j == 1:
                    color = colors[Field.SCORE_HOW_MANY]
                    width = middle_width
                else:
                    color = colors[Field.DATE]
                    width = default_width
                self.placeLabel(self.topFrame, str(temp[i][j]), color, width=width, row=i, column=j)

    # read and display last score
    def refresh_last(self):
        temp = self.db.get_last(1)
        for j in range(2):
            if j == 0:
                color = colors[Field.SCORE_REACTION_TIME]
                width = default_width
            elif j == 1:
                color = colors[Field.SCORE_HOW_MANY]
                width = middle_width
            else:
                color = colors[Field.SCORE_DATE]
                width = default_width
            self.placeLabel(self.lastFrame, str(temp[0][j]), color, width=width, row=0, column=j)

    # reload database scores and refresh histogram
    def refresh_histogram(self):
        self.chart = plt.Figure(figsize=(chart_size_x, chart_size_y), dpi=100)
        self.canvas = FigureCanvasTkAgg(self.chart, self.histogramFrame)
        self.canvas.get_tk_widget().grid(row=0, column=0, sticky="NSEW")
        self.ax = self.chart.add_subplot(111)
        
        df = DataFrame(self.db.get_scores())
        self.ax.clear()
        df.plot.hist(bins=35, legend=False, ax=self.ax)
        # df.plot(kind='hist', legend=False, ax=self.ax)
        # self.ax = df.plot.hist()
        self.ax.set_title(self.text[Field.CHART_TITLE])
        self.ax.set_xlabel(self.text[Field.CHART_X])
        self.ax.set_ylabel(self.text[Field.CHART_Y])
        self.master.update()
        print("refresh")

    def exit(self, event):
        self.master.destroy()

    # for saving space in the code
    def placeLabel(self, frame, text="xd", background="olive drab", width=default_width, fontsize=fontsize, row=0, column=0):
        Label(frame, text=text, background=background, width=width, height="0", font=('arial', fontsize))\
            .grid(row=row, column=column, sticky="NSEW")

    #changing instructions label and refreshing results
    def process(self):
        # if True:
        #     line = "b'ready\\r\\n'"
        if self.arduino.isOpen():
            line = str(self.arduino.readline())
            line = line[2:line.find('\\')]
            if "ready" in line:
                self.placeLabel(self.master, self.text[Field.INSTRUCTION_IDLE],
                                colors[Field.INSTRUCTION_IDLE], row=self.instruction_row)
            elif "preparing" in line:
                self.placeLabel(self.master, self.text[Field.INSTRUCTION_PREPARING],
                                colors[Field.INSTRUCTION_PREPARING], row=self.instruction_row)
            elif "running" in line:
                self.placeLabel(self.master, self.text[Field.INSTRUCTION_RUNNING],
                                colors[Field.INSTRUCTION_RUNNING], row=self.instruction_row)
            elif "False" in line:
                self.placeLabel(self.master, self.text[Field.INSTRUCTION_FALSE_START],
                                colors[Field.INSTRUCTION_FALSE_START], row=self.instruction_row)
            elif "fail" in line:
                self.placeLabel(self.master, self.text[Field.INSTRUCTION_TIMEOUT],
                                colors[Field.INSTRUCTION_TIMEOUT], row=self.instruction_row)
            elif line.isdecimal():
                self.placeLabel(self.master, self.text[Field.INSTRUCTION_STOP] + line + " \u03BCs",
                                    colors[Field.INSTRUCTION_STOP], row=self.instruction_row)
                self.db.save_score(line)
                self.refresh_histogram()
                self.placeLabel(self.master,
                    text=self.text[Field.TOP_SCORES] + " (" + str(self.db.get_number_of_results()) + self.text[Field.TOTAL_SCORES],
                    background=colors[Field.TOP_SCORES],
                    width=default_width,
                    fontsize=fontsize,
                    row=self.top_scores_row)
                self.refresh_top()
                self.refresh_last()
            else:
                self.placeLabel(self.master, self.text[Field.INSTRUCTION_ERROR],
                                colors[Field.INSTRUCTION_ERROR], row=self.instruction_row)
        self.master.after(100, self.process)
