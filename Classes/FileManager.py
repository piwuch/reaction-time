import os.path
import csv
import math
from datetime import datetime

class FileManager:
    def __init__(self, filename = "testfile.csv"):
        self.filename = filename
        self.scores = {}
        if not os.path.isfile(filename):
            with open(filename, 'w+') as db:
                writer = csv.writer(db, delimiter='\t')
                writer.writerow(['Date and Time', 'Score'])
                writer.writerow(['2018-09-14  13:27:00', '450000'])
                writer.writerow(['2018-09-14  13:27:01', '460000'])
                writer.writerow(['2018-09-14  13:27:02', '470000'])
        self.refresh()

    def refresh(self):
        self.scores = {}
        with open(self.filename, 'r') as db:
            reader = csv.reader(db, delimiter='\t')
            count = 0
            for row in reader:
                if count == 0:
                    count += 1
                    continue
                elif len(row) == 0:
                    count += 1
                    continue
                else:
                    self.scores[row[0]] = int(row[1])
                    count += 1

    def get_scores(self):
        scores = []
        for score in self.scores.values():
            if score <= 500000:
                scores.append(round(score/1000000, 3))
        #print(scores)
        return scores

    def get_top(self, amount=3):
        # text = "Top " + str(amount) + "\n"
        db = {}
        scoreTab = []
        for date, score in self.scores.items():
            db[date] = score
        #print(db)
        scores = {}
        for i in range(amount):
            dates_to_delete = []
            minimal = 1000000
            for date, score in db.items():
                if minimal > score:
                    minimal = score
            for date, score in db.items():
                if score == minimal:
                    scores[date] = score
                    # text += str(score) + "\t" + date + "\n"
                    scoreTab.append([str(round(score/1000000, 3)), str(math.ceil(score/72.2)), date])
                    dates_to_delete.append(date)
                    if len(scores) >= amount:
                        return scoreTab
            for date in dates_to_delete:
                del db[date]
        return scoreTab

    def get_last(self, amount=3):
        scoreTab = []
        # text = "Last " + str(amount) + "\n"
        dates = sorted(self.scores.keys(), reverse=True)
        for i in range(amount):
            scoreTab.append([str(round(self.scores[dates[i]]/1000000, 3)), str(math.ceil(self.scores[dates[i]]/72.2)), dates[i]])
            # text += str(self.scores[dates[i]]) + "\t" + dates[i] + "\n"
        return scoreTab

    def get_all(self):
        return self.scores

    def save_score(self, score):
        with open(self.filename, 'a') as db:
            writer = csv.writer(db, delimiter='\t')
            writer.writerow(['{:%Y-%m-%d %H:%M:%S}'.format(datetime.now()), score])
        self.refresh()

    def get_number_of_results(self):
        return len(self.scores)
