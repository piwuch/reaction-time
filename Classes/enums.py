from enum import Enum, auto

class Language(Enum):
    ENGLISH = auto()
    FRENCH = auto()
    POLISH = auto()

class Field(Enum):
    REACTION_TIME = auto()
    HOW_MANY = auto()
    DATE = auto()
    TOP_SCORES = auto()
    TOTAL_SCORES = auto()
    SCORE_REACTION_TIME = auto()
    SCORE_HOW_MANY = auto()
    SCORE_DATE = auto()
    LAST_SCORE = auto()
    INSTRUCTION_IDLE = auto()
    INSTRUCTION_PREPARING = auto()
    INSTRUCTION_RUNNING = auto()
    INSTRUCTION_STOP = ()
    INSTRUCTION_TIMEOUT = auto()
    INSTRUCTION_FALSE_START = auto()
    INSTRUCTION_ERROR = auto()
    CHART_TITLE = auto()
    CHART_X = auto()
    CHART_Y = auto()
