#include <Arduino.h>

//states
typedef enum {
    IDLE,
    PREPARING,
    RUNNING,
	TIMEOUT
}machine_state;
machine_state state = IDLE;

//pinout
int green_led_pin =   12;
int yellow_led_pin =  11;
int red_led_pin =     10;
int mushroom_pin =     9;
int gnd_pin =      8;

//variables
unsigned int preparing_time[] = {7, 9, 5, 6, 6, 8, 6 ,9 ,7 ,6 ,8 ,10, 6, 9, 10, 6, 5, 5, 7, 8};
int time_counter = 0;
int countdown = 0;
unsigned long stopwatch_start = 0;
unsigned long stopwatch_stop = 0;
boolean falseStartFlag = 0;

void setup() {
  Serial.begin(115200);
  Serial.println("ready");

  pinMode(green_led_pin, OUTPUT);
  pinMode(yellow_led_pin, OUTPUT);
  pinMode(red_led_pin, OUTPUT);
  pinMode(mushroom_pin, INPUT_PULLUP);
  pinMode(gnd_pin, OUTPUT);

  digitalWrite(green_led_pin, HIGH);
  digitalWrite(gnd_pin, LOW);
}

void loop() {
switch(state){
case IDLE:{
	if (digitalRead(mushroom_pin) == LOW){
		state = PREPARING;
		Serial.println("preparing");
		digitalWrite(green_led_pin, LOW);
		digitalWrite(yellow_led_pin, HIGH);
		countdown = preparing_time[time_counter++] * 1000;
		delay(100);
    if (time_counter == 20)
      time_counter = 0;
    }
} break;

case PREPARING:{
	falseStartFlag = 0;
	for(; countdown > 0; countdown--){
		delay(1);
		if(digitalRead(mushroom_pin) == HIGH){
			Serial.println("False Start!");
			state = IDLE;
			digitalWrite(yellow_led_pin, LOW);
			for(int i = 0; i < 58; i++){
				digitalWrite(green_led_pin, (digitalRead(green_led_pin)) ^ 1);
				digitalWrite(yellow_led_pin, (digitalRead(yellow_led_pin)) ^ 1);
				digitalWrite(red_led_pin, (digitalRead(red_led_pin)) ^ 1);
				delay(100);
			}
			digitalWrite(green_led_pin, HIGH);
			digitalWrite(yellow_led_pin, LOW);
      Serial.println("ready");
			falseStartFlag = 1;
			break;
		}
	}
	if(falseStartFlag == 0){
		state = RUNNING;
		digitalWrite(yellow_led_pin, LOW);
		digitalWrite(red_led_pin, HIGH);
		Serial.println("running");
		stopwatch_start = micros();
	}
} break;

case RUNNING:{
	stopwatch_stop = micros();
	unsigned long time = 0;
	if (stopwatch_start < stopwatch_stop){
		time = stopwatch_stop - stopwatch_start;
	} else{
		stopwatch_start = 4294967295 - stopwatch_start;
		time = stopwatch_stop + stopwatch_start;
	}
	if (time > 5000000){
		Serial.println("fail");
		digitalWrite(red_led_pin, LOW);
		state = TIMEOUT;
		break;

	}
	if(digitalRead(mushroom_pin) == HIGH){
		Serial.println(time);
		state = IDLE;
		digitalWrite(red_led_pin, LOW);
		digitalWrite(green_led_pin, HIGH);
    delay(3000);
    Serial.println("ready");
  }
} break;

case TIMEOUT:{
	if(digitalRead(mushroom_pin) == HIGH){
		state = IDLE;
		Serial.println("ready");
		digitalWrite(green_led_pin, HIGH);
    delay(100);
	}
} break;
}}
